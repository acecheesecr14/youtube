package eu.ac3_servers.dev.youtube.bukkit;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

public class YouTube extends JavaPlugin implements PluginMessageListener {
	
	public final static String channelName = "YouTube";

	@Override
	public void onEnable() {
		
		getServer().getMessenger().registerOutgoingPluginChannel(this, channelName);
		getServer().getMessenger().registerIncomingPluginChannel(this, channelName, this);
		
	}

	@Override
	public void onPluginMessageReceived(String channel, Player player,
			byte[] data) {
		
		if(!channel.equalsIgnoreCase(channelName)) return;
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		try {
			String command = dis.readUTF();
			
			if(command.equalsIgnoreCase("bounce")){
				
				int times = dis.readInt();
				
				getLogger().info("Received BOUNCE data : " + times);
				
				if(times <= 0){
					return;
				}
				
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				DataOutputStream dos = new DataOutputStream(baos);
				
				dos.writeUTF(command);
				dos.writeInt(times - 1);
				
				Player receiver = getServer().getOnlinePlayers().iterator().next();
				
				receiver.sendPluginMessage(this, channelName, baos.toByteArray());
				
			}else{
				System.err.println("Something has gone wrong and we received an invalid message!");
			}
			
		} catch (IOException e1) {
			e1.printStackTrace();
			System.err.println("The end of the world!");
		}
		
	}
	
}

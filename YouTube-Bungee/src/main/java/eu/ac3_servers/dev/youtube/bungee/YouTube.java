package eu.ac3_servers.dev.youtube.bungee;

import eu.ac3_servers.dev.youtube.bungee.commands.BounceCommand;
import eu.ac3_servers.dev.youtube.bungee.commands.HelloCommand;
import eu.ac3_servers.dev.youtube.bungee.commands.ResponderCommand;
import eu.ac3_servers.dev.youtube.bungee.listeners.PluginMessageResponder;

public class YouTube extends YouTubePlugin {
	
	public final static String channelName = "YouTube";
	
	@Override
	public void onEnable() {
		
		getProxy().registerChannel(channelName);
		
		getLogger().info("HI!");
		
		//Commands
		new HelloCommand(this);
		new BounceCommand(this);
		new ResponderCommand(this);
		
		//Listeners
		new PluginMessageResponder(this);
		
	}

}

package eu.ac3_servers.dev.youtube.bungee;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.config.Configuration;

public abstract class YouTubeCommand extends Command {

	private YouTube plugin;

	public YouTubeCommand(YouTube plugin, String name) {
		super(name);
		this.plugin = plugin;
		
		getProxy().getPluginManager().registerCommand(getPlugin(), this);
		getPlugin().getLogger().info("Registered command:" + getName());
	}
	
	public YouTubeCommand(YouTube plugin, String name, String permission) {
		super(name, permission, new String[0]);
		this.plugin = plugin;
		
		getProxy().getPluginManager().registerCommand(getPlugin(), this);
		getPlugin().getLogger().info("Registered command:" + getName());
	}
	
	public YouTubeCommand(YouTube plugin, String name, String permission, String[] aliases) {
		super(name, permission, aliases);
		this.plugin = plugin;
		getProxy().getPluginManager().registerCommand(getPlugin(), this);
		getPlugin().getLogger().info("Registered command:" + getName());
	}
	
	protected YouTube getPlugin(){
		return this.plugin;
	}
	
	protected ProxyServer getProxy(){
		return this.getPlugin().getProxy();
	}
	
	protected Configuration getConfig(){
		return this.getPlugin().getConfig();
	}
	
	protected void saveConfig(){
		this.getPlugin().saveConfig();
	}

}

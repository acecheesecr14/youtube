package eu.ac3_servers.dev.youtube.bungee;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.config.Configuration;

public class YouTubeListener implements Listener {
	
	private YouTube plugin;
	private String name;

	public YouTubeListener(YouTube plugin, String name) {
		
		this.plugin = plugin;
		this.name = name;
		
		getProxy().getPluginManager().registerListener(getPlugin(), this);
		getPlugin().getLogger().info("Registered Listener" + this.getClass().getName());
		
	}
	
	protected YouTubePlugin getPlugin(){
		return this.plugin;
	}
	
	protected ProxyServer getProxy(){
		return this.getPlugin().getProxy();
	}
	
	protected String getName(){
		return this.name;
	}
	
	protected Configuration getConfig(){
		return getPlugin().getConfig();
	}
	
	protected void saveConfig(){
		getPlugin().saveConfig();
	}

}

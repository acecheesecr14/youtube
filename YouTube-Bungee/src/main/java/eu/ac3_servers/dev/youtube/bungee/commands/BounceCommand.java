package eu.ac3_servers.dev.youtube.bungee.commands;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;

import eu.ac3_servers.dev.youtube.bungee.YouTube;
import eu.ac3_servers.dev.youtube.bungee.YouTubeCommand;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.config.ServerInfo;

public class BounceCommand extends YouTubeCommand {

	public BounceCommand(YouTube youTube) {
		super(youTube, "bounce", "youtube.bounce", new String[0]);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(args.length != 1){
			
			sender.sendMessage("Usage: /bounce NumberOfTimes");
			return;
			
		}
		
		int NumberOfTimes = Integer.parseInt(args[0]);
		if(NumberOfTimes < 1) NumberOfTimes = 1;
		if(NumberOfTimes > 20) NumberOfTimes = 15;
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		
		
		try {
			
			dos.writeUTF("BOUNCE");
			dos.writeInt(NumberOfTimes);
			
			Map<String, ServerInfo> servers = getProxy().getServers();
			
			for (ServerInfo server : servers.values()) {
				
				server.sendData(YouTube.channelName, baos.toByteArray());
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}

package eu.ac3_servers.dev.youtube.bungee.commands;

import eu.ac3_servers.dev.youtube.bungee.YouTube;
import eu.ac3_servers.dev.youtube.bungee.YouTubeCommand;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class HelloCommand extends YouTubeCommand {

	public HelloCommand(YouTube youTube) {
		
		super(youTube, "hello", null, new String[]{
				"hey",
				"hi",
				"haii"
		});
		
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(!(sender instanceof ProxiedPlayer)){
			
			//If the sender isn't a player say "go away!"
			sender.sendMessage("This command can't be done from console! Sorry.");
			return;
			
		}
		
		ProxiedPlayer player = (ProxiedPlayer) sender;
		
		TextComponent tc0 = new TextComponent("Oh hello ");
		tc0.addExtra(player.getName());
		tc0.setBold(true);
		tc0.setColor(ChatColor.GREEN);
		
		player.sendMessage(tc0);

	}

}

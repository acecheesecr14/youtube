package eu.ac3_servers.dev.youtube.bungee.commands;

import net.md_5.bungee.api.CommandSender;
import eu.ac3_servers.dev.youtube.bungee.YouTube;
import eu.ac3_servers.dev.youtube.bungee.YouTubeCommand;

public class ResponderCommand extends YouTubeCommand {

	public ResponderCommand(YouTube plugin) {
		super(plugin, "responder", "youtube.responder");
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(CommandSender sender, String[] args) {
		
		if(getConfig().getBoolean("main.responder")){
			
			getConfig().set("main.responder", false);
			saveConfig();
			sender.sendMessage("Responding to bounce messages turned off!");
			return;
			
		}else{
			
			getConfig().set("main.responder", true);
			saveConfig();
			sender.sendMessage("Responding to bounce messages turned on!");
			
		}

	}

}

package eu.ac3_servers.dev.youtube.bungee.listeners;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;

import eu.ac3_servers.dev.youtube.bungee.YouTube;
import eu.ac3_servers.dev.youtube.bungee.YouTubeListener;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.event.EventHandler;

public class PluginMessageResponder extends YouTubeListener {

	public PluginMessageResponder(YouTube youTube) {
		super(youTube, "PluginMessageResponder");
	}
	
	@EventHandler
	public void onPluginMessage(PluginMessageEvent e){
		
		if(!getConfig().getBoolean("main.responder")){
			return;
		}
		
		if(!e.getTag().equalsIgnoreCase(YouTube.channelName)) return;
		
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(e.getData()));
		
		try {
			String command = dis.readUTF();
			
			if(command.equalsIgnoreCase("bounce")){
				
				int times = dis.readInt();
				
				getPlugin().getLogger().info("Received BOUNCE data : " + times);
				
				if(times <= 0){
					return;
				}
				
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				DataOutputStream dos = new DataOutputStream(baos);
				
				dos.writeUTF(command);
				dos.writeInt(times - 1);
				
				Map<String, ServerInfo> servers = getProxy().getServers();
				
				for (ServerInfo server : servers.values()) {
					
					server.sendData(YouTube.channelName, baos.toByteArray());
					
				}
				
			}else{
				System.err.println("Something has gone wrong and we received an invalid message!");
			}
			
		} catch (IOException e1) {
			e1.printStackTrace();
			System.err.println("The end of the world!");
		}
		
	}

}
